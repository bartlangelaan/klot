# Klot

Klot is a incredibly lightweight (600 bytes gzip+min) translation module with built in variable interpolation written in Typescript. Meet typed translations. It's just a simple factory function that produces a `translate` function. Translations are supplied using objects or JSON and can either be a simple string or a complex object that can be transformed using transformation function.

## Installation

### NPM

`npm install klot`

### Yarn

`yarn add klot`

## Usage

To start translating, Klot must be initialized. Import Klot and supply it a locale and a locale map that defines your translations per locale. Use the returned translation function to start translating.

A more thorough documentation is given later on in this README.

### Basic usage

```ts
import klot from "klot";
import nl from "./nl.json";

const translate = klot("en", {
  // Object literal
  en: {
    "Hello, world": "Hello, beautiful world",
    Strawberries: "Strawberries"
  },
  // JSON import
  nl
});

const helloWorld = translate("Hello, world"); // => "Hello, beautiful world"

const strawberries = translate("Strawberries"); // => "Strawberries"

const unknown = translate("Watermelons"); // TS: Type Error, => "Watermelons" - JS: => "Watermelons"
```

### Interpolation

Lets say you need to use one or more variables inside of your translation. Simply add values to the options object argument to replace the variable name with an actual variable. The syntax is `{variableName}`, but can be changed to a different RegExp.

```ts
const translate = klot("en", {
  en: { "T-shirt: {color}": "T-shirt: {color}" }
});

const tshirtOne = translate("T-shirt: {color}", { values: { color: "Red" } }); // => "T-shirt: Red

const tshirtTwo = translate("T-shirt: {color}", { values: { color: "Blue" } }); // => "T-shirt: Blue
```

You can also choose to use global variables. Note: local variables will overwrite global variables.

```ts
const translate = klot(
  "en",
  {
    en: { "Copyright {brand} {currentYear}": "Copyright {brand} {currentYear}" }
  },
  { values: { brand: "My Brand", currentYear: new Date().getFullYear() } }
);

const copyright = translate("Copyright {brand} {currentYear}"); // => "Copyright My Brand 2019" for example

const copyrightShorthand = translate("T-shirt: {color}", {
  values: { brand: "M.B." }
}); // => "Copyright M.B. 2019" for example
```

### Transformations

Transformations are functions in Klot that can be used to apply logic to your translations.
They make translations extensible and smart. A translation's value can be either a simple string or an object of strings. Whenever you use an object as a translation value, the translate function returns function instead of a string. The translation becomes curryable with a transformation function that can be used to determine which translation within the object should be used.

```ts
const translate = klot("en", {
  // Object literal
  en: {
    "{amount} Strawberries": {
      none: "No strawberries",
      singular: "One strawberry",
      plural: "{amount} strawberries"
    }
  }
});

function pluralize<
  I extends { none: string; singular: string; plural: string },
  O extends KlotOptions & { values: { amount: number } }
>(input: I, options: O) {
  if (options.values.amount > 0) {
    return input.plural;
  }

  if (options.values.amount === 1) {
    return input.singular;
  }

  return input.none;
}

const noStrawberries = translate("{amount} strawberries", {
  values: { amount: 0 }
})(pluralize); // => "No strawberries"

const oneStrawberries = translate("{amount} strawberries", {
  values: { amount: 1 }
})(pluralize); // => "One strawberry"

const multipleStrawberries = translate("{amount} strawberries", {
  values: { amount: 18 }
})(pluralize); // => "18 strawberries"
```

## Options

**TBD**

## Contributing

**TBD**
